﻿using Api.DB;
using Api.DTO;
using Api.Enums;
using Api.Interfaces;
using Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Api.Services
{
    public class WeatherService : IWeatherService
    {
        public async Task<IEnumerable<Weather>> GetWeather()
        {
            IEnumerable<Weather> result = null;
            using (WeatherContext ctx = new WeatherContext())
            {
                result = await ctx.WeatherData
                          .OrderByDescending(x => x.Time)
                          .Take(10)
                          .ToListAsync();
            }
            return result;
        }

        public async Task<Weather> CityWeather(string city)
        {
            Weather result = null;
            using (WeatherContext ctx = new WeatherContext())
            {
                result = await ctx.WeatherData
                            .Include(x => x.City)
                            .OrderByDescending(x => x.Time)
                            .FirstOrDefaultAsync(x => x.City.Name == city);
            }
            return result;
        }

        public async Task<IEnumerable<string>> GetCityList()
        {
            IEnumerable<string> result = null;
            using (WeatherContext ctx = new WeatherContext())
            {
                result = await ctx.Cities
                   .Select(x => x.Name)
                   .ToListAsync();
            }
            return result;
        }

        private WeatherCondition MetaWeatherStateConverter(string metaWeatherState)
        {
            switch(metaWeatherState)
            {
                case "c":
                case "lc":
                    return WeatherCondition.Sunny;
                case "hc":
                    return WeatherCondition.Cloudy;
                case "s":
                case "lr":
                case "hr":
                    return WeatherCondition.Rainy;
                case "sn":
                case "sl":
                    return WeatherCondition.Snowy;
                default:
                    return WeatherCondition.Unknown;
            }
        }

        public async Task<bool> UpdateWeather()
        {
            bool result = false;
            using (HttpClient client = new HttpClient())
            {
                using (WeatherContext ctx = new WeatherContext())
                {
                    foreach (City city in await ctx.Cities.ToListAsync())
                    {
                        HttpResponseMessage locationResponse = await client.GetAsync($"https://www.metaweather.com/api/location/search/?lattlong={city.Latitude.ToString("0.0000")},{city.Longitude.ToString("0.0000")}");
                        if (locationResponse.IsSuccessStatusCode)
                        {
                            MetaWeatherLocation location = (await locationResponse.Content
                                                                           .ReadAsAsync<List<MetaWeatherLocation>>())
                                                                           .FirstOrDefault();

                            if (location == null)
                                continue;

                            HttpResponseMessage weatherResponse = await client.GetAsync($"https://www.metaweather.com/api/location/{location.woeid}/");
                            if (weatherResponse.IsSuccessStatusCode)
                            {
                                MetaWeather mw = await weatherResponse.Content.ReadAsAsync<MetaWeather>();

                                if (mw?.consolidated_weather == null)
                                    continue;

                                if (!mw.consolidated_weather.Any())
                                    continue;

                                MetaWeatherElement mwe = mw.consolidated_weather.First();

                                Weather weather = new Weather()
                                {
                                    City = city,
                                    Conditions = MetaWeatherStateConverter(mwe.weather_state_abbr),
                                    Temperature = mwe.the_temp,
                                    WindDirection = mwe.wind_direction,
                                    WindSpeed = mwe.wind_speed * 0.44704,   // conversion from miles per hour to meters per second
                                    Time = DateTime.UtcNow
                                };
                                ctx.WeatherData.Add(weather);
                            }
                        }

                    }
                    result = await ctx.SaveChangesAsync() > 0;
                }
            }
            return result;
        }
    }
}