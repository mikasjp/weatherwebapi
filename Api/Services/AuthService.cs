﻿using Api.DB;
using Api.Interfaces;
using Api.Misc;
using Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;

namespace Api.Services
{
    public class AuthService : IAuthService
    {
        public async Task<string> Authorise(string username, string password)
        {
            if (String.IsNullOrWhiteSpace(username) || String.IsNullOrWhiteSpace(password))
                return null;

            string result = null;

            using (WeatherContext ctx = new WeatherContext())
            {
                User user = await ctx.Users
                                     .SingleOrDefaultAsync(x => x.Username == username);

                if(user != null)
                {
                    if(VerifyPassword(password, user.Password))
                    {
                        result = JwtManager.GenerateToken(user.Username);
                    }
                }
            }

            return result;
        }

        private string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        private bool VerifyPassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password, hash);
        }
    }
}