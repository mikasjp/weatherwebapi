﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Api.DB
{
    public class WeatherContext: DbContext
    {
        public WeatherContext(): base()
        {
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Weather> WeatherData { get; set; }
        public DbSet<User> Users { get; set; }
    }
}