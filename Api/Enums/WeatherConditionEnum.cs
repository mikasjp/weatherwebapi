﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Enums
{
    public enum WeatherCondition
    {
        Sunny = 0,
        Cloudy = 1,
        Rainy = 2,
        Snowy = 3,
        Unknown = 4
    }
}