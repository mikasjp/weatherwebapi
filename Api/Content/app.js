﻿// WEATHER PANEL
new Vue({
    el: '#cityWeather',
    data: {
        loading: true,
        weather: null,
        cityList: null,
        selectedCity: null
    },
    computed: {
        time: function () {
            let date = new Date(this.weather.Time);
            return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
        },
        weatherConditions: function () {
            switch (this.weather.Conditions) {
                case 0:
                    return 'Sunny';
                case 1:
                    return 'Cloudy';
                case 2:
                    return 'Rainy';
                case 3:
                    return 'Snowy';
                default:
                    return 'Unknown';
            }
        }
    },
    methods: {
        onCitySelectorChange: function (event) {
            this.selectedCity = event.target.value;
        },
        onRefreshButtonClick: function () {
            if (!this.selectedCity)
                return;

            axios
                .get(`api/weather/${this.selectedCity}`)
                .then(response => {
                    this.weather = response.data
                });
        }
    },
    filters: {
        formatNumber: function (value) {
            return value.toFixed(2);
        }
    },
    mounted() {
        axios
            .get('api/city')
            .then(response => {
                this.cityList = response.data;
                this.selectedCity = this.cityList[0];
                this.loading = false;
            });
    }
})

// ADMIN PANEL
new Vue({
    el: '#admin',
    data: {
        authenticated: false,
        username: null,
        password: null,
        success: null,
        error: null,
        loading: false,
        updating: false
    },
    methods: {
        onLoginButtonClick: function () {
            this.error = null;
            this.loading = true;
            axios
                .get(`api/auth/login/${this.username}/${this.password}`)
                .then(response => {
                    sessionStorage.setItem("token", response.data.Token);
                    this.authenticated = true;
                    this.loading = false;
                    this.username = null;
                    this.password = null;
                })
                .catch(error => {
                    let status = error.response.status;
                    if (status == 401) {
                        this.error = "Invalid username and/or password";
                    } else {
                        this.error = "Unhandled error";
                    }
                    this.loading = false;
                });
        },
        onLogoutButtonClick: function () {
            sessionStorage.removeItem("token");
            this.authenticated = false;
        },
        onUpdateButtonClick: function () {
            this.updating = true;
            let token = sessionStorage.getItem("token");
            axios
                .get('api/weather/update', { headers: { Authorization: `Bearer ${token}`} })
                .then(response => {
                    console.log(response);
                    if (response.data) {
                        this.success = 'Weather updated successfully!';
                        this.updating = false;
                        setTimeout(() => { this.success = null; }, 2500);
                    }
                })
                .catch(error => {
                    let status = error.response.status;
                    if (status == 401) {
                        this.error = 'Your session has expired';
                        this.authenticated = false;
                    }
                    else {
                        this.error = 'Unhandled error';
                        setTimeout(() => { this.error = null; }, 2500);
                    }
                    this.updating = false;
                });
        }
    },
    mounted() {
        if (sessionStorage.getItem("token"))
            this.authenticated = true;
    }
})
