﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.DTO
{
    public class MetaWeatherElement
    {
        public long id { get; set; }
        public string weather_state_name { get; set; }
        public string weather_state_abbr { get; set; }
        public DateTime created { get; set; }
        public double the_temp { get; set; }
        public double wind_speed { get; set; }
        public double wind_direction { get; set; }
    }
}