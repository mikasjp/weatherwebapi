﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.DTO
{
    public class MetaWeather
    {
        public List<MetaWeatherElement> consolidated_weather { get; set; }
        public string title { get; set; }
    }
}
