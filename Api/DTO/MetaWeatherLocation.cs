﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.DTO
{
    public class MetaWeatherLocation
    {
        public double distance { get; set; }
        public string title { get; set; }
        public long woeid { get; set; }
    }
}