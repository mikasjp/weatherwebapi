﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.DTO
{
    public class JwtDto
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}