﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Interfaces
{
    public interface IAuthService
    {
        Task<string> Authorise(string user, string pass);
    }
}
