﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Interfaces
{
    public interface IWeatherService
    {
        Task<IEnumerable<Weather>> GetWeather();
        Task<Weather> CityWeather(string city);
        Task<IEnumerable<string>> GetCityList();
        Task<bool> UpdateWeather();
    }
}
