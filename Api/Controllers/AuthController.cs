﻿using Api.DTO;
using Api.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Api.Controllers
{
    public class AuthController : ApiController
    {
        private IAuthService _service;

        public AuthController(IAuthService service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/auth/login/{username}/{password}")]
        public async Task<IHttpActionResult> Login(string username, string password)
        {
            string token = await _service.Authorise(username, password);
            if (token == null)
            {
                return Unauthorized();
            }
            else
            {
                return Ok(new JwtDto()
                {
                    Username = username,
                    Token = token
                });
            }
        }
    }
}
