﻿using Api.Filters;
using Api.Interfaces;
using Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Api.Controllers
{
    public class WeatherController : ApiController
    {
        IWeatherService _service;

        public WeatherController(IWeatherService service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/weather")]
        public async Task<IHttpActionResult> All() => Ok(await _service.GetWeather());

        [AllowAnonymous]
        [HttpGet]
        [Route("api/city")]
        public async Task<IHttpActionResult> CityList() => Ok(await _service.GetCityList());

        [AllowAnonymous]
        [HttpGet]
        [Route("api/weather/{city}")]
        public async Task<IHttpActionResult> City(string city)
        {
            var w = await _service.CityWeather(city);
            if (w == null)
                return NotFound();
            else
                return Ok(w);
        }

        [JwtAuthentication]
        [HttpGet]
        [Route("api/weather/update")]
        public async Task<IHttpActionResult> UpdateWeather()
        {
            var result = await _service.UpdateWeather();
            return Ok(result);
        }
    }
}
