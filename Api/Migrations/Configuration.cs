﻿namespace Api.Migrations
{
    using Api.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Api.DB.WeatherContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Api.DB.WeatherContext context)
        {
            context.Cities.AddOrUpdate(x => x.Id,
                new City() { Id = 1, Name = "Warsaw", Latitude = 52.2297, Longitude = 21.0122 },
                new City() { Id = 2, Name = "London", Latitude = 51.5074, Longitude = 0.1278 },
                new City() { Id = 3, Name = "Berlin", Latitude = 52.52, Longitude = 13.405 },
                new City() { Id = 4, Name = "NYC", Latitude = 40.7128, Longitude = -74.006 },
                new City() { Id = 5, Name = "Melbourne", Latitude = -37.8136, Longitude = 144.9631 }
                );

            context.WeatherData.AddOrUpdate(x => x.Id,
                new Weather() { Id = 1, CityId = 1, Conditions = Enums.WeatherCondition.Sunny, Temperature = 20.0, Time = DateTime.UtcNow, WindDirection = 0.0, WindSpeed = 2.0 },
                new Weather() { Id = 2, CityId = 2, Conditions = Enums.WeatherCondition.Cloudy, Temperature = 15.0, Time = DateTime.UtcNow, WindDirection = 20.0, WindSpeed = 5.0 },
                new Weather() { Id = 3, CityId = 3, Conditions = Enums.WeatherCondition.Sunny, Temperature = 22.0, Time = DateTime.UtcNow, WindDirection = 40.0, WindSpeed = 1.0 },
                new Weather() { Id = 4, CityId = 4, Conditions = Enums.WeatherCondition.Rainy, Temperature = 10.0, Time = DateTime.UtcNow, WindDirection = 60.0, WindSpeed = 0.5 }
            );

            context.Users.AddOrUpdate(x => x.Id,
                new User() { Id = 1, Username = "admin", Password = BCrypt.Net.BCrypt.HashPassword("password")}    
            );
        }
    }
}
