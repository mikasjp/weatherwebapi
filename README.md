# Weather API example using ASP.NET WebApi 2

## 1. Overview

This is example simple SPA application with ASP.NET WebApi 2 backend and Vue.js frontend.

## 2. Backend

### 2.1 ASP.NET WebApi 2

As mentioned above, ASP.NET WebApi 2 is the heart of the application. It is used to handle HTTP API requests coming to the application and their appropriate routing.
Two controllers were used:

* WeatherController - is responsible for handling requests related to access to weather data (all users) and their updates (logged in admins).
* AuthController - responsible for user authentication and issuing JWT tokens.

### 2.2 Entity Framework

EF6 was used as a data access layer. The advantage of this solution is the ability to easily switch between databases. In the case of this application, a test database built into Visual Studio is used, but nothing stands in the way of using a different database, e.g. MSSQL or MySQL.
The "[code first](https://docs.microsoft.com/pl-pl/aspnet/mvc/overview/getting-started/getting-started-with-ef-using-mvc/creating-an-entity-framework-data-model-for-an-asp-net-mvc-application)" approach was used. Database tables are built on the basis of previously prepared data models.  There is a possibility of a preliminary population of the database.

### 2.3 Authorisation

JWT tokens were used as the authentication mechanism. They have the advantage that the backend does not have to store user session information. Inside the token, you can store a lot of information, such as the user's role in the system. In the case of this application, it was assumed that the ordinary user is a person not logged in and the administrator is a logged-in person.
The guarantor of the security of this solution is the secret value stored on the backend side. You can read more about JWT tokens at [https://jwt.io/](https://jwt.io/).

### 2.4 Dependency Injection

The [Autofac](https://autofac.org/) library was used as the [dependency injection](https://en.wikipedia.org/wiki/Dependency_injection) container. The created application is small and basically there was no need to use DI, but it was done for demonstration reasons.

### 2.5 Users secrets storage

User login data is stored in the database. There is no need to encrypt the username, but the password must be stored securely. The password is stored in the form of a salted [bcrypt hash](https://en.wikipedia.org/wiki/Bcrypt). This solution guarantees relatively high security even in the case of sensitive data leakage.

### 2.6. Integration with external API

As an example of integration with the external API, the [MetaWeather API](https://www.metaweather.com/api/) was used, which is available for free without the need for authorization. The data returned by the MetaWeather API is available in JSON format. Data received in this way is deserialized to DTO objects and then converted to the local data format. Data collected in this way is saved in the database.

## 3. Frontend

The application was created as a so-called SPA (single page application). To achieve this, the [Vue.js framework](https://vuejs.org/) has been used. [The Axios library](https://github.com/axios/axios) was used to communicate with the backend API. The application stores the JWT token of logged in users in [the sessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage) of the browser.

## 4. Usage

1. Open Weather.sln in Visual Studio,
2. Execute Update-Databse command in Package Manager Console to create tables and populate DB,
3. Run application,
4. Enjoy!

Default login credentials:

* **Username:** admin
* **Password:** password

## 5. API endpoints

### 5.1 Weather list

Returns last 10 weather records.

`GET /api/weather`

### 5.2 Cities list

Returns the list of cities for which weather information is available.

`GET /api/city`

### 5.3 City weather

Returns the last weather information for a specific city.

`GET /api/weather/(city)`

### 5.4 Update weather information **(authorisation required)**

Updates weather information for cities from the 5.2 cities list.

`GET /api/weather/update`

### 5.5 Authorise user

Authorises user. If credentials are correct, returns JWT token valid for 30 minutes. Otherwise returns HTTP 401 (Unauthorised) response code.

`GET /api/auth/authorize?username=(username)&password=(password)`

## 6. Possibilities for future development

* Use "[Repository](https://docs.microsoft.com/pl-pl/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application)" design pattern,
* Add unit tests
